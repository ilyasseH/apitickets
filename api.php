<?php
header('Content-Type: application/json');
try {
   $pdo = new PDO('mysql:host=localhost;port=3306;dbname=tickets;','root','');
   $retour["success"]= true;
   $retour["message"]="connexion à la base de données reussie";
}
catch(Exception $e){
   $retour["success"]= false;
   $retour["message"]="connexion à la base de données échouée";
}


// POST:Pour tester sur POSTMAN id = 14-18 + operation = delete
if($_POST["operation"] == "delete"){
	
    $requete = $pdo->prepare("DELETE FROM `ticket` WHERE `ticket`.`id` LIKE :v");
    $requete->bindParam(':v',$_POST["id"]);
    $requete->execute();
	}

// operation = add  identifiant,date,description,severite
else if($_POST["operation"] == "add" && !empty($_POST["identifiant"]) && !empty($_POST["date"]) && !empty($_POST["description"]) && !empty($_POST["severite"]) ){
	$requete = $pdo->prepare("INSERT INTO `ticket` (`id`, `identifiant`, `date`, `description`, `severite`) VALUES (NULL,:a, :b, :c, :d)");
    $requete->bindParam(':a',$_POST["identifiant"]);
    $requete->bindParam(':b',$_POST["date"]);
    $requete->bindParam(':c',$_POST["description"]);
    $requete->bindParam(':d',$_POST["severite"]);
    $requete->execute();
    
	}
	//operation = upload  id=14-18,identifiant,date,description,severite
else if($_POST["operation"] == "upload" && !empty($_POST["identifiant"]) && !empty($_POST["date"]) && !empty($_POST["description"]) && !empty($_POST["severite"])){
	$requete = $pdo->prepare("UPDATE `ticket` SET `identifiant` = :a, `date` = :b, `description` = :c, `severite` = :d WHERE `ticket`.`id` = :i ");
	$requete->bindParam(':i',$_POST["id"]);
    $requete->bindParam(':a',$_POST["identifiant"]);
    $requete->bindParam(':b',$_POST["date"]);
    $requete->bindParam(':c',$_POST["description"]);
    $requete->bindParam(':d',$_POST["severite"]);
    $requete->execute();
}
//Envoie des listes des tickets à jour en JSON
$requete = $pdo->prepare("SELECT * FROM ticket");
$requete->execute();
$resultat=$requete->fetchAll();
$retour["liste"]=$resultat;
echo json_encode($retour);





