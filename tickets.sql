-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 07, 2020 at 08:29 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

﻿--
-- Database: `tickets`
--
﻿
-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `identifiant` varchar(100) CHARACTER SET latin1 NOT NULL,
  `date` date NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `severite` varchar(10) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;
﻿
--
-- Dumping data for table `ticket`
--

﻿INSERT INTO `ticket` (`id`, `identifiant`, `date`, `description`, `severite`) VALUES
(18, 'BACK', '2020-03-09', 'il fait beau aujourdhui', 'medium')﻿,
(17, 'BACK', '2020-03-09', 'Ebola mÃªme', 'low')﻿,
(15, 'front', '2020-03-08', 'api testÃ© par postman', 'high')﻿,
(14, 'front', '2020-03-07', 'acceuil platform e\'commerce blablablav', 'medium')﻿;
﻿COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
